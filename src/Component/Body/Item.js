import React, { Component } from 'react'

export default class Item extends Component {
  render() {
    return (
      <div id='bodyItem' className='row'>
        <div className='col-4 item'>
          <div className='itemIcon bg-primary text-white'>
          <i class="bi bi-collection"></i>
          </div>
          <div className='itemContent rounded'>
            <h2>Fresh new layout</h2>
            <p className='mb-0'>With Bootstrap 5, we've created a fresh new layout for this template!</p>
          </div>
        </div>
        <div className='col-4 item'>
          <div className='itemIcon bg-primary text-white'>
          <i class="bi bi-cloud-download"></i>
          </div>
          <div className='itemContent rounded'>
            <h2>Free to download</h2>
            <p className='mb-0'>As always, Start Bootstrap has a powerful collectin of free templates.</p>
          </div>
        </div>
        <div className='col-4 item'>
          <div className='itemIcon bg-primary text-white'>
          <i class="bi bi-card-heading"></i>
          </div>
          <div className='itemContent rounded'>
            <h2>Jumbotron hero header</h2>
            <p className='mb-0'>The heroic part of this template is the jumbotron hero header!</p>
          </div>
        </div>
        <div className='col-4 item'>
          <div className='itemIcon bg-primary text-white'>
          <i class="bi bi-bootstrap"></i>
          </div>
          <div className='itemContent rounded'>
            <h2>Feature boxes</h2>
            <p className='mb-0'>We've created some custom feature boxes using Bootstrap icons!</p>
          </div>
        </div>
        <div className='col-4 item'>
          <div className='itemIcon bg-primary text-white'>
          <i class="bi bi-code"></i>
          </div>
          <div className='itemContent rounded'>
            <h2>Simple clean code</h2>
            <p className='mb-0'>We keep our dependencies up to date and squash bugs as they come!</p>
          </div>
        </div>
        <div className='col-4 item'>
          <div className='itemIcon bg-primary text-white'>
          <i class="bi bi-patch-check"></i>
          </div>
          <div className='itemContent rounded'>
            <h2>A name you trust</h2>
            <p className='mb-0'>Start Bootstrap has been the leader in free Bootstrap templates since 2013!</p>
          </div>
        </div>
      </div>
    )
  }
}
