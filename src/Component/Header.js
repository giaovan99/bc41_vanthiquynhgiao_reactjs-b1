import React, { Component } from "react";
import "../css/header.css"

export default class Header extends Component {
  render() {
    return (
      <header className="bg-dark">
        <nav className="navbar navbar-expand-lg navbar-dark border" >
          <div>
              <a className="navbar-brand" href="#">
                Start Bootstrap
              </a>
          </div>
          <div className="collapse navbar-collapse" id="navbarNav">
            <ul className="navbar-nav">
              <li className="nav-item active">
                <a className="nav-link" href="#">
                  Home <span className="sr-only">(current)</span>
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  About
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link" href="#">
                  Contact
                </a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    );
  }
}
